"""Contains tests for fizzbuzz.py."""


# Python Standard Library
import unittest


# First-party Library
import fizzbuzz


class TestFizzBuzz(unittest.TestCase):
    """Test various aspects of the fizzbuzz module."""

    def test_individual_num(self):
        """Test that FizzBuzz of an individual number is that number."""
        self.assertEqual("1", fizzbuzz.get_individual_fizzbuzz(1))

    def test_individual_fizz(self):
        """Test that FizzBuzz of a multiple of three returns "Fizz"."""
        self.assertEqual(
            fizzbuzz.Constants.FIZZ_STRING,
            fizzbuzz.get_individual_fizzbuzz(fizzbuzz.Constants.FIZZ_DIVISOR),
        )

    def test_individual_buzz(self):
        """Test that FizzBuzz of a multiple of three returns "Buzz"."""
        self.assertEqual(
            fizzbuzz.Constants.BUZZ_STRING,
            fizzbuzz.get_individual_fizzbuzz(fizzbuzz.Constants.BUZZ_DIVISOR),
        )

    def test_individual_fizzbuzz(self):
        """Test that FizzBuzz of a multiple of both returns "FizzBuzz"."""
        expected_result = (
            fizzbuzz.Constants.FIZZ_STRING + fizzbuzz.Constants.BUZZ_STRING
        )
        fizzbuzz_divisor = (
            fizzbuzz.Constants.FIZZ_DIVISOR * fizzbuzz.Constants.BUZZ_DIVISOR
        )
        self.assertEqual(
            expected_result, fizzbuzz.get_individual_fizzbuzz(fizzbuzz_divisor)
        )

    def test_individual_zero_raises_exception(self):
        """Test that FizzBuzz of Zero raises ValueError."""
        self.assertRaises(ValueError, fizzbuzz.get_individual_fizzbuzz, 0)

    def test_individual_negative_raises_exception(self):
        """Test that FizzBuzz of a negative raises ValueError."""
        self.assertRaises(ValueError, fizzbuzz.get_individual_fizzbuzz, -1)

    def test_individual_none_raises_exception(self):
        """Test that FizzBuzz of None raises ValueError."""
        self.assertRaises(ValueError, fizzbuzz.get_individual_fizzbuzz, None)

    def test_individual_string_raises_exception(self):
        """Test that FizzBuzz of a string raises ValueError."""
        self.assertRaises(ValueError, fizzbuzz.get_individual_fizzbuzz, "One")

    def test_fizzbuzz_to_fifteen(self):
        """Test the result of FizzBuzz up to fifteen."""
        expected_result = [
            "1",
            "2",
            "Fizz",
            "4",
            "Buzz",
            "Fizz",
            "7",
            "8",
            "Fizz",
            "Buzz",
            "11",
            "Fizz",
            "13",
            "14",
            "FizzBuzz",
        ]
        self.assertEqual(expected_result, list(fizzbuzz.get_fizzbuzz(15)))


if __name__ == "__main__":
    unittest.main()
