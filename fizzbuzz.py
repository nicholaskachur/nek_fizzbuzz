"""Implements FizzBuzz algorithm and associated helper methods."""


import sys


class Constants:
    EXIT_SUCCESS = 0
    EXIT_FAILURE = 1
    FIZZ_DIVISOR = 3
    BUZZ_DIVISOR = 5
    FIZZ_STRING = "Fizz"
    BUZZ_STRING = "Buzz"


def get_individual_fizzbuzz(num):
    """Return the FizzBuzz of a single number."""
    if not isinstance(num, int) or num < 1:
        raise ValueError("Argument must be an integer greater than zero.")
    is_fizz = num % Constants.FIZZ_DIVISOR == 0
    is_buzz = num % Constants.BUZZ_DIVISOR == 0
    if is_fizz and is_buzz:
        return Constants.FIZZ_STRING + Constants.BUZZ_STRING
    elif is_fizz:
        return Constants.FIZZ_STRING
    elif is_buzz:
        return Constants.BUZZ_STRING
    return str(num)


def get_fizzbuzz(max_num):
    """Return an iterable of FizzBuzz results up to a given number."""
    if not isinstance(max_num, int) or max_num < 1:
        raise ValueError("Argument must be an integer greater than zero.")
    return map(get_individual_fizzbuzz, range(1, max_num + 1))


def fizzbuzz(max_num):
    """Print FizzBuzz out to a given number."""
    if not isinstance(max_num, int) or max_num < 1:
        raise ValueError("Argument must be an integer greater than zero.")
    for i in get_fizzbuzz(max_num):
        print(i)


def usage(program_name):
    """Print usage."""
    print("usage: python {} <max_num>".format(program_name))


def main():
    """Return status code after printing FizzBuzz based on argv."""
    if len(sys.argv) != 2:
        usage(sys.argv[0])
        return Constants.EXIT_FAILURE

    try:
        max_num = int(sys.argv[1])
        if max_num < 1:
            raise ValueError()
    except ValueError:
        usage(sys.argv[0])
        print("error: max_num must be an integer greater than zero")
        return Constants.EXIT_FAILURE

    fizzbuzz(max_num)
    return Constants.EXIT_SUCCESS


if __name__ == "__main__":
    sys.exit(main())
