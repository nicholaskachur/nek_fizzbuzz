# nek_fizzbuzz

A sample implementation of the classic programming exercise.

## Quick Start

Run with a Python 3 interpreter (tested in CPython 3.7.0) with:

```
python fizzbuzz.py <max_num>
```

This will print out FizzBuzz to the provided maximum, which should be an
integer greater than zero. Usage information is printed otherwise.

Run the unit tests with a similar command:

```
python tests.py
```

If you change the Constants enum in `fizzbuzz.py` you may need to alter
`tests.py` to match.

## Rules of FizzBuzz

FizzBuzz is an elementary school game wherein children take turns
saying the numbers one through one hundred. On every multiple of three,
the current player says "Fizz" instead of their number. Likewise, on
every multiple of five the current player is to say "Buzz" instead of
their number. Finally, on every multiple of both three and five (that
is, every multiple of fifteen), the player is to say "FizzBuzz".

This is a classic coding exercise used to assess programming skill.
It generally tests the usage of conditional statements and loops, among
other techniques.

## Discussion

My solution is a mildly-mannered stand-alone Python script, which can be
run at the command-line to produce a game of FizzBuzz.

It makes use of several constants to determine the divisors and strings
to use for Fizz and Buzz. The meat of the implementation is the function
`get_individual_fizzbuzz(num)`, which produces a Fizz, Buzz, FizzBuzz,
or the string of the integer as appropriate. A FizzBuzz game can then be
produced by using the function `get_fizzbuzz(max_num)`, which maps the
above function to the range specified. The `fizzbuzz(max_num)` function
simply prints this out, and is used to produce the output of the script.

Error handling is performed in each function to ensure the argument is a
positve non-zero integer, and incorrect input to the script prints usage
information.

This is maybe a little more than necessary, but I tried to decompose the
solution as I might do for a more complex problem. There is currently no
packaging here, as that's not the intended goal of this sample.

I'm also proud to say that, while not strictly necessary for such a
small project, I made use of the Black Python Formatter and PyLint tools
in the creation of this code. The only ignored PyLint warnings are those
for the `fizzbuzz.Constants` class, which is intended to replicate an
enum in other languages, and as such doesn't need an init method or
member functions.

## Performance

This code performs quickly for small values of n, but on my 2014 laptop
it started to run quite slowly once n grew beyond a millon or so. If you
do find it running overly long, it should respond promptly to a Ctrl-C
keyboard interrupt.

## Contact

I don't imagine this is necessary, but feel free to fork, open issues,
or create pull requests as you desire.

I remain yours,

_Nicholas Kachur_
